<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function timeZone($location){
		return date_default_timezone_set($location);
    }

    public function dashboard()
    {
        $data_user = DB::table('users')->paginate(10);

        return view('home.dashboard'
        , compact('data_user'));
    }

    
public function store(Request $request)
{		
    $date = date("Y-m-d H:i:s"); 

	DB::table('users')->insert([
		'name' => $request->name,
		'email' => $request->email,
        'password' => bcrypt($request->password),
        'created_at' => $date
	]);
	return redirect('/dashboard');

}

public function edit($id)
{
	$data_user = DB::table('users')->where('id',$id)->get();
    return view('home.edit'
    , compact('data_user'));

}

public function update(Request $request)
{
    $date = date("Y-m-d H:i:s"); 

	DB::table('users')->where('id',$request->id)->update([
		'name' => $request->name,
		'email' => $request->email,
		'password' => $request->password,
		'updated_at' => $date
	]);
	return redirect('/dashboard');
}

public function hapus($id)
{
	DB::table('users')->where('id',$id)->delete();
		
	return redirect('/dashboard');
}
}

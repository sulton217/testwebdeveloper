<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use Session;

class AuthController extends Controller
{
    //
    public function timeZone($location){
		return date_default_timezone_set($location);
    }

    public function login(){
        if(session()->has('login')){
            Session::flash('belum_logout','Anda Belum Logout');
            return redirect('/dashboard');
        }else{
            return view('Auth.login');
        }
    }

    public function LoginProses(Request $request){
        $data = User::where('email', $request->email)->first();
        if($data != null && $request->password != null){ 
            $decrypt = Hash::check($request->password,$data->password); 
            if($data->email == $request->email && $decrypt == $request->password){ 
                session()->put('email', $data->email); 
                session()->put('login',TRUE); 

                Session::flash('sukses','Berhasil Login ');
                return redirect('/dashboard'); 
            }else{
                Session::flash('salah','Email atau Password Salah !');
                return redirect('/login'); 
            }
        }else{
            Session::flash('kosong','Mohon Lengkapi Email da Password !');
            return redirect('/login');
        }
    }

   

    public function register_akun()
    {

        return view('Auth.register');
    }

    public function register(Request $request)
{		
    $date = date("Y-m-d H:i:s"); 

	DB::table('users')->insert([
		'name' => $request->name,
		'email' => $request->email,
        'password' => bcrypt($request->password),
        'created_at' => $date
    ]);
    Session::flash('register','Berhasil , Akun Telah Terdaftar , Silahkan Login !');
	return redirect('/login');

}

public function logout(){
    session()->flush();
    Session::flash('logout','Akun Anda Berhasil Logout !');
    return redirect('/login');
}

}

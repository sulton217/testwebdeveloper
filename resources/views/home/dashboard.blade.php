@extends('layouts.master')
@section('content') </br>
@if ($message = Session::get('belum_logout'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif
@if ($message = Session::get('sukses'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $message }}</strong>
  </div>
@endif
<h5>Halaman Dashboard > <span class="badge badge-secondary">Data User  </span></h5>
</br>
<div class="container" >
  
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Di Daftarkan Pada Tanggal </th>
      <th scope="col">Opsi</th>

    </tr>
  </thead>
  <tbody>
@foreach($data_user as $user)
    <tr>
      <th scope="row"></th>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->created_at}}</td>
      <td>
          <a href="/user/edit/{{ $user->id }}"><button type="button" class=" btn btn-outline-secondary">Edit</button> </a>
          <a href="/user/hapus/{{ $user->id }}"><button type="button" class=" btn btn-outline-secondary">Hapus</button></a> 

      </td>

    </tr>
@endforeach

  </tbody>
</table>
</div>

<!-- Pop Up atau Modal Tambah  -->
<div class="modal fade" id="tambah" tabindex="-1" aria-labelledby="ModalTambah" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambahkan Users</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
          <form name="form" action="/store" method="post">
         {{csrf_field()}}
              <div class="modal-body">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama</label>
                      <input type="text" class="form-control"  name="name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email </label>
                      <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
                      <small id="emailHelp" class="form-text text-muted">Contoh : JhonDhoe@gmail.com</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control"  name="password">
                    </div>
                    
              </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambahkan User</button>
      </div>
      </form>

    </div>
  </div>
</div>
@stop
@extends('layouts.master')
@section('content') </br>
@foreach($data_user as $data)
<h5>Halaman > <span class="badge badge-secondary">Edit Data User  </span></h5>
</br>
<div class="container" >
<form name="form" action="/update" method="post">
         {{csrf_field()}}

<div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama</label>
    <div class="col-sm-10">
      <input type="name" class="form-control" value="{{$data->name}}">
    </div>
</div>

<div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" value="{{$data->email}}">
    </div>
</div>

<div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" value="{{$data->password}}">
    </div>
</div>
<button type="submit" class="btn btn-secondary btn-lg btn-block">Edit Data User</button>

</form>
@endforeach
@stop
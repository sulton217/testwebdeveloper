<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Auth.login');
});
Route::get('/register_akun','AuthController@register_akun');
Route::post('/register','AuthController@register');
Route::get('/login','AuthController@login');
Route::post('/postlogin','AuthController@LoginProses');
Route::get('/logout','AuthController@logout');


Route::group(['middleware' => 'CekSession'], function () {
    ////////////////////////////////////////////////////////////////////////////////////

    Route::post('/store','HomeController@store');
    Route::get('/user/edit/{id}','HomeController@edit');
    Route::post('/user/update','HomeController@update');
    Route::get('/user/hapus/{id}','HomeController@hapus');
    Route::get('/dashboard','HomeController@dashboard');

    
});

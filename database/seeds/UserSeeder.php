<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function timeZone($location){
		return date_default_timezone_set($location);
    }

    public function run()
    {
        //
        $date = date("Y-m-d H:i:s"); 

        DB::table('users')->insert([
        	'name' => 'AdminJhonDHoe',
        	'email' => 'admin@gmail.com',
        	'password' => 'admin123',
        	'created_at' => $date
        ]);
        DB::table('users')->insert([
        	'name' => 'Sulton',
        	'email' => 'sulton@gmail.com',
        	'password' => 'sulton123',
        	'created_at' => $date
        ]);
        DB::table('users')->insert([
        	'name' => 'UserNew',
        	'email' => 'user@gmail.com',
        	'password' => 'user123',
        	'created_at' => $date
        ]);
    }
}
